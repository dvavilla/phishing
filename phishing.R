spam <- read.csv("Phishing.csv")
set.seed(1500)
ind <- sample(1:2256, 1500)
train <- spam[-ind,]
test <- spam[ind,]
trainDecision <- spamDecisions[-ind]

spamDecisions <- spam[,13]
View(spamData)
spamData <- spam[,-13]
trainDecision <- spamDecisions[-ind]
testDecision <- spamDecisions[ind]
# Hierarchical clustering -------------------------------------------------
scale_Spam <-  scale(spamData) # scaling the data set
distmat <- dist(scale_Spam) # creating the distance matrix
# creating 3 different hierarchical clusters with complete, single and average linkages
completeLink <- hclust(distmat, method="complete") 
singleLink <- hclust(distmat, method="single")
averageLink <- hclust(distmat, method="average")
# plots of the 3 clusters
plot(completeLink)
plot(averageLink)
plot(singleLink)
# cutting the trees at k=2, since there should be 2 groups
sres <- cutree(singleLink, k=2)
ares <- cutree(averageLink, k=2)
cres <- cutree(completeLink, k=2)

spam <- read.csv("Phishing.csv")
set.seed(1500)
ind <- sample(1:2256, 1500)
train <- spam[-ind,]
test <- spam[ind,]
trainDecision <- spamDecisions[-ind]

spamDecisions <- spam[,13]
View(spamData)
spamData <- spam[,-13]
trainDecision <- spamDecisions[-ind]
testDecision <- spamDecisions[ind]
# Hierarchical clustering -------------------------------------------------
scale_Spam <-  scale(spamData) # scaling the data set
distmat <- dist(scale_Spam) # creating the distance matrix
# creating 3 different hierarchical clusters with complete, single and average linkages
completeLink <- hclust(distmat, method="complete") 
singleLink <- hclust(distmat, method="single")
averageLink <- hclust(distmat, method="average")
# plots of the 3 clusters
plot(completeLink)
plot(averageLink)
plot(singleLink)
# cutting the trees at k=2, since there should be 2 groups
sres <- cutree(singleLink, k=2)
ares <- cutree(averageLink, k=2)
cres <- cutree(completeLink, k=2)

# mis-classification table for the 3 kinds of hierarchical clusters 
table(sres, spamDecisions)
table(ares, spamDecisions)
table(cres, spamDecisions)
# Single linkage misclassification rate = 
# Average linkage misclassification rate = 
# Complete linkage misclassification rate = 
# Variable selection for LDA -------------------------------------------
library(MASS)

attach(train)

# create a linear model with the training data set
simlm <- lm(Phishy ~., data=train)
summary(simlm)
attach(spam)
# create a linear model with the entire data set
simlmFull <- lm(Phishy~., data = spam)
summary(simlmFull)

# use the stepAIC funcion to perform backward variable selection on the training and entire data set's linear models
step <- stepAIC(simlm, direction = "backward")
fullStep <- stepAIC(simlmFull, direction = "backward")

# LDA with model from Variable selection for LDA -------------------------------------------
# create linear discriminant analysis model with the training linear model previously created
ldamod <- lda(step$call$formula, data = train)
ldaResult <- predict(ldamod, newdata = test[,-13]) # predict response values for the test set
table(ldaResult$class, testDecision) # mis-classification table
# LDA cross-validation ----------------------------------------------------
library(MASS)
ldacv <- lda(test[,-13], testDecision, CV=TRUE) # lda model with cross validation
table(testDecision, ldacv$class)  # mis-classification table
library(gclus)
library(MASS)
library(class)

# perform knn classification with k = 5
kresults <- knn(train[,-13],test[,-13],train[,13],k=5)
kresults
linearMod<- lm(URLs ~ HTML_iFrames, data=train)
summary(linearMod)

linearMod2 <- lm( URLs ~ Encoding, data=train)
View(linearMod2)
print(linearMod2)

summary(linearMod2)

